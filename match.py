from bs4 import BeautifulSoup
from selenium import webdriver
def match(driver, html_tag, expr, 
          attrs={}, context=None, tag_only=False):
    soup = BeautifulSoup(driver.page_source, features='html.parser')
    source = soup if context is None else context
    
    for tag in source.find_all(html_tag, attrs=attrs):
        if expr in tag.decode_contents():
            if tag_only:
                return tag
            return soup.find_all(html_tag).index(tag)